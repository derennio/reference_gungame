package de.ennio20.gungame.util;

import com.google.common.base.Preconditions;
import de.ennio20.gungame.GunGame;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

/**
 * @author Ennio20
 */
public class User {
    private GunGame plugin;
    private Player spigotPlayer;
    private int level;
    private GameMode gameMode;

    /**
     * Constructor for the GunGame user
     *
     * @param spigotPlayer Owning player
     */
    public User( Player spigotPlayer ) {
        Preconditions.checkNotNull( spigotPlayer );

        this.spigotPlayer = spigotPlayer;
        this.level = 0;
        this.gameMode = GameMode.INGAME;
        this.plugin = GunGame.getInstance();

        plugin.getUsers().add( this );

        this.spigotPlayer.getAttribute( Attribute.GENERIC_ATTACK_SPEED ).setBaseValue( 100D );

        addLevel();

    }

    /**
     * @return owning player
     */
    public Player getSpigotPlayer ( ) {
        return spigotPlayer;
    }

    /**
     * @return user gamemode
     */
    public GameMode getGameMode ( ) {
        return gameMode;
    }

    /**
     * @return user level
     */
    public int getLevel ( ) {
        return level;
    }

    /**
     * Simple method to add level and update equipment
     */
    public void addLevel( ) {
        this.level += 1;

        getSpigotPlayer().playSound( getSpigotPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1 );
        getSpigotPlayer().setLevel( this.level );

        if ( level > 5 )
            return;

        setLevelItems();

    }

    /**
     * This method removes a specified amount of level (playerdeath etc.)
     *
     * @param amount to remove specified amount of level
     */
    public void removeLevel( int amount ) {
        if ( this.level > 1 )
            this.level -= amount;

        setLevelItems();

    }

    /**
     * Method to change player Gamemode
     *
     * @param gameMode which will be assigned to user
     */
    public void setGameMode ( GameMode gameMode ) {
        Preconditions.checkNotNull( gameMode );

        this.gameMode = gameMode;
    }

    /**
     * Method to send messages with prefix without instance call in every message
     *
     * @param message which will be sent to the player
     */
    public void sendMessage( Object message ) {
        getSpigotPlayer().sendMessage( plugin.getPrefix() + message );

    }

    /**
     * This method sets the specified items of an level to the player.
     */
    public void setLevelItems( ) {
        String s = String.valueOf( this.level );

        if ( this.level > 5 )
            s = "5";

        Material combat = Material.matchMaterial( plugin.getLevelsFile().get( s ).getAsString() );

        Bukkit.getScheduler().runTaskAsynchronously( plugin, ( ) -> {
            ItemStack itemStack = new ItemStack( combat );
            ItemMeta meta = itemStack.getItemMeta();
            meta.setUnbreakable( true );
            itemStack.setItemMeta( meta );

            getSpigotPlayer().getInventory().clear();
            getSpigotPlayer().getInventory().setItem( 0, itemStack );
            getSpigotPlayer().setLevel( this.level );
            getSpigotPlayer().setVelocity( new Vector( 0, 0, 0 ) );
            getSpigotPlayer().playSound( getSpigotPlayer().getLocation(), Sound.BLOCK_ANVIL_HIT, 1, 1 );

        } );

    }

    public enum GameMode {
        INGAME,
        SPECTATING

    }

}
