/*
 * initially source from https://gist.github.com/KingFaris10/4527fbaf8caa9fd7b800
 */
package de.ennio20.gungame.util;

import com.google.common.base.Preconditions;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.util.Vector;

import java.util.*;

public class Region implements Cloneable, ConfigurationSerializable, Iterable< Block > {
    private String worldName;
    private final Vector minimumPoint, maximumPoint;

    /**
     * Constructs a new region from another
     *
     * @param region old region
     */
    private Region ( Region region ) {
        this( region.worldName, region.minimumPoint.getX( ), region.minimumPoint.getY( ), region.minimumPoint.getZ( ),
                region.maximumPoint.getX( ), region.maximumPoint.getY( ), region.maximumPoint.getZ( ) );
    }

    /**
     * Constructs a one-block-region
     *
     * @param loc target location
     */
    public Region ( Location loc ) {
        this( loc, loc );
    }

    /**
     * Constructs a region from two locations
     *
     * @param loc1 target location 1 (e.g. top corner)
     * @param loc2 target location 2 (e.g. bottom corner)
     */
    public Region ( Location loc1, Location loc2 ) {
        Preconditions.checkNotNull( loc1, "Could not find first location!" );
        Preconditions.checkNotNull( loc2, "Could not find second location!" );
            if ( loc1.getWorld( ) != null && loc2.getWorld( ) != null ) {
                if ( !loc1.getWorld( ).getUID( ).equals( loc2.getWorld( ).getUID( ) ) )
                    throw new IllegalStateException( "The 2 locations of the cuboid must be in the same world!" );
            } else {
                throw new NullPointerException( "One/both of the worlds is/are null!" );
            }
            this.worldName = loc1.getWorld( ).getName( );

            double xPos1 = Math.min( loc1.getX( ), loc2.getX( ) );
            double yPos1 = Math.min( loc1.getY( ), loc2.getY( ) );
            double zPos1 = Math.min( loc1.getZ( ), loc2.getZ( ) );
            double xPos2 = Math.max( loc1.getX( ), loc2.getX( ) );
            double yPos2 = Math.max( loc1.getY( ), loc2.getY( ) );
            double zPos2 = Math.max( loc1.getZ( ), loc2.getZ( ) );
            this.minimumPoint = new Vector( xPos1, yPos1, zPos1 );
            this.maximumPoint = new Vector( xPos2, yPos2, zPos2 );

    }

    /**
     * Constructs a region from coordinates
     *
     * @param worldName target world
     * @param x1        coordinate
     * @param y1        coordinate
     * @param z1        coordinate
     * @param x2        coordinate
     * @param y2        coordinate
     * @param z2        coordinate
     */
    private Region ( String worldName, double x1, double y1, double z1, double x2, double y2, double z2 ) {
        if ( worldName == null || Bukkit.getServer( ).getWorld( worldName ) == null )
            throw new NullPointerException( "One/both of the worlds is/are null!" );
        this.worldName = worldName;

        double xPos1 = Math.min( x1, x2 );
        double xPos2 = Math.max( x1, x2 );
        double yPos1 = Math.min( y1, y2 );
        double yPos2 = Math.max( y1, y2 );
        double zPos1 = Math.min( z1, z2 );
        double zPos2 = Math.max( z1, z2 );
        this.minimumPoint = new Vector( xPos1, yPos1, zPos1 );
        this.maximumPoint = new Vector( xPos2, yPos2, zPos2 );
    }

    /**
     * Check if a location is inside a region
     *
     * @param location target location
     * @return boolean
     */
    public boolean containsLocation ( Location location ) {
        return location != null && location.getWorld( ).getName( ).equals( this.worldName ) && location.toVector( )
                .isInAABB( this.minimumPoint, this.maximumPoint );
    }

    /**
     * Check if a vector is inside a region
     *
     * @param vector target vector
     * @return boolean
     */
    public boolean containsVector ( Vector vector ) {
        return vector != null && vector.isInAABB( this.minimumPoint, this.maximumPoint );
    }

    /**
     * Get every block in the region
     *
     * @return list with blocks
     */
    public List< Block > getBlocks ( ) {
        List< Block > blockList = new ArrayList<>( );
        World world = this.getWorld( );

        Preconditions.checkNotNull( world );

            for ( int x = this.minimumPoint.getBlockX( ); x <= this.maximumPoint.getBlockX( ); x++ ) {
                for ( int y = this.minimumPoint.getBlockY( ); y <= this.maximumPoint.getBlockY( ) && y <= world.getMaxHeight( ); y++ ) {
                    for ( int z = this.minimumPoint.getBlockZ( ); z <= this.maximumPoint.getBlockZ( ); z++ ) {
                        blockList.add( world.getBlockAt( x, y, z ) );
                    }
                }
            }
        return blockList;
    }

    /**
     * @return lower corner
     */
    public Location getLowerLocation ( ) {
        return this.minimumPoint.toLocation( this.getWorld( ) );
    }

    /**
     * @return lower x
     */
    public double getLowerX ( ) {
        return this.minimumPoint.getX( );
    }

    /**
     * @return lower y
     */
    public double getLowerY ( ) {
        return this.minimumPoint.getY( );
    }

    /**
     * @return lower z
     */
    public double getLowerZ ( ) {
        return this.minimumPoint.getZ( );
    }

    /**
     * @return upper corner
     */
    public Location getUpperLocation ( ) {
        return this.maximumPoint.toLocation( this.getWorld( ) );
    }

    /**
     * @return upper x
     */
    public double getUpperX ( ) {
        return this.maximumPoint.getX( );
    }

    /**
     * @return upper y
     */
    public double getUpperY ( ) {
        return this.maximumPoint.getY( );
    }

    /**
     * @return upper z
     */
    public double getUpperZ ( ) {
        return this.maximumPoint.getZ( );
    }

    /**
     * @return region area
     */
    public double getVolume ( ) {
        return ( this.getUpperX( ) - this.getLowerX( ) + 1 ) *
                ( this.getUpperY( ) - this.getLowerY( ) + 1 ) *
                ( this.getUpperZ( ) - this.getLowerZ( ) + 1 );
    }

    /**
     * @return region world
     */
    public World getWorld ( ) {
        World world = Bukkit.getServer( ).getWorld( this.worldName );

        Preconditions.checkNotNull( world );

        return world;
    }

    /**
     * Method to change the target world
     *
     * @param world new world
     */
    public void setWorld ( World world ) {
        Preconditions.checkNotNull( world );

        this.worldName = world.getName( );
    }

    @Override
    public Region clone ( ) {
        return new Region( this );
    }

    @Override
    public ListIterator< Block > iterator ( ) {
        return this.getBlocks( ).listIterator( );
    }

    @Override
    public Map< String, Object > serialize ( ) {
        Map< String, Object > serializedCuboid = new HashMap<>( );
        serializedCuboid.put( "worldName", this.worldName );
        serializedCuboid.put( "x1", this.minimumPoint.getX( ) );
        serializedCuboid.put( "x2", this.maximumPoint.getX( ) );
        serializedCuboid.put( "y1", this.minimumPoint.getY( ) );
        serializedCuboid.put( "y2", this.maximumPoint.getY( ) );
        serializedCuboid.put( "z1", this.minimumPoint.getZ( ) );
        serializedCuboid.put( "z2", this.maximumPoint.getZ( ) );
        return serializedCuboid;
    }

    /**
     * Method to deserialize a region (e.g. file-stored regions)
     *
     * @param serializedCuboid map with information
     * @return region
     */
    public static Region deserialize ( Map< String, Object > serializedCuboid ) {
        try {
            String worldName = ( String ) serializedCuboid.get( "worldName" );

            double xPos1 = ( Double ) serializedCuboid.get( "x1" );
            double xPos2 = ( Double ) serializedCuboid.get( "x2" );
            double yPos1 = ( Double ) serializedCuboid.get( "y1" );
            double yPos2 = ( Double ) serializedCuboid.get( "y2" );
            double zPos1 = ( Double ) serializedCuboid.get( "z1" );
            double zPos2 = ( Double ) serializedCuboid.get( "z2" );

            return new Region( worldName, xPos1, yPos1, zPos1, xPos2, yPos2, zPos2 );
        } catch ( Exception ex ) {
            ex.printStackTrace( );
            return null;
        }
    }

}
