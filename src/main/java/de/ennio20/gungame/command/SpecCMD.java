package de.ennio20.gungame.command;

import de.ennio20.gungame.GunGame;
import de.ennio20.gungame.util.User;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author Ennio20
 */
public class SpecCMD implements CommandExecutor {
    /**
     * Constructs the command
     */
    public SpecCMD( ) {
        GunGame.getInstance().getCommand( "spec" ).setExecutor( this );

    }

    @Override
    public boolean onCommand ( CommandSender commandSender, Command command, String label, String[] args ) {
        if ( !(commandSender instanceof Player ) )
            return false;

        final Player player = ( Player ) commandSender;
        final User user = GunGame.getInstance().getUser( player );

        if ( user.getGameMode().equals( User.GameMode.INGAME ) ) {
            if ( !GunGame.getInstance().getSpawn().containsLocation( player.getLocation() ) ) {
                user.sendMessage( "§cDu musst am Spawn sein, um diesen Befehl nutzen zu können!" );
                return false;

            }

            user.setGameMode( User.GameMode.SPECTATING );

            for ( Player all : Bukkit.getOnlinePlayers() )
                all.hidePlayer( player );

            player.setAllowFlight( true );
            player.setFlying( true );

            Bukkit.getScheduler().runTaskAsynchronously( GunGame.getInstance(), ( ) -> {
                ItemStack compass = new ItemStack( Material.COMPASS );
                ItemMeta meta = compass.getItemMeta();
                meta.setDisplayName( "§7Tracker" );
                compass.setItemMeta( meta );

                player.getInventory().clear();
                player.getInventory().setItem( 0, compass );

            } );

            GunGame.getInstance().getSpectator().add( user );

            user.sendMessage( "Du befindest dich nun im Spectatormodus!" );

        } else {
            final Location location = new Location( Bukkit.getWorld( "world" ), 1033.5, 56, 191.5 );
            player.teleport( location );

            user.setGameMode( User.GameMode.INGAME );

            for ( Player all : Bukkit.getOnlinePlayers() )
                all.showPlayer( player );

            player.setAllowFlight( false );
            player.setFlying( false );

            user.setLevelItems();

            GunGame.getInstance().getSpectator().removeIf( x -> x.getSpigotPlayer() == player );

            user.sendMessage( "Du befindest dich nun im Ingamemodus!" );
        }

        return false;
    }
}