package de.ennio20.gungame.event;

import de.ennio20.gungame.GunGame;
import de.ennio20.gungame.util.User;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

/**
 * @author Ennio20
 */
public class DamageHandler implements Listener {
    /**
     * Constructs the listener
     */
    public DamageHandler( ) {
        GunGame.getInstance().getServer().getPluginManager().registerEvents( this, GunGame.getInstance() );

    }

    @EventHandler
    public void onRespawn( PlayerRespawnEvent event ) {
        final Player player = event.getPlayer();
        final User playerUser = GunGame.getInstance().getUser( player );
        final Player killer = player.getKiller();
        final Location location = new Location( Bukkit.getWorld( "world" ), 1033.5, 56, 191.5 );

        event.setRespawnLocation( location );

        playerUser.removeLevel( 1 );

        if ( killer != null ) {
            final User killerUser = GunGame.getInstance().getUser( killer );
            killerUser.addLevel();
            killerUser.sendMessage( "Du hast §a" + player.getName() + "§7 getötet." );

            playerUser.sendMessage( "Du wurdest von §a" + killer.getName() + "§7 getötet." );

        } else {
            playerUser.sendMessage( "Du bist gestorben." );

        }

    }

    @EventHandler
    public void onDeath( PlayerDeathEvent event ) {
        final Player player = event.getEntity();

        event.setDeathMessage( null );
        event.setDroppedExp( 0 );
        event.getDrops().clear();

        player.spigot().respawn();

    }

    @EventHandler
    public void onDamageByEntity( EntityDamageByEntityEvent event ) {
        if ( event.getDamager() instanceof Player && event.getEntity() instanceof Player ) {
            final Player player = ( Player ) event.getEntity( );
            final Player damager = ( Player ) event.getDamager( );

            if ( GunGame.getInstance( ).getSpectator( ).contains( GunGame.getInstance( ).getUser( damager ) ) ) {
                event.setCancelled( true );
                return;

            }

            if ( GunGame.getInstance().getSpawn().containsLocation( player.getLocation() ) || GunGame.getInstance().getSpawn().containsLocation( damager.getLocation() ) )
                event.setCancelled( true );

        }

    }

    @EventHandler
    public void onEntityDamage( EntityDamageEvent event ) {
        if ( event.getEntity() instanceof Player ) {
            final Player player = ( Player ) event.getEntity();
            final User user = GunGame.getInstance( ).getUser( player );

            if ( GunGame.getInstance( ).getSpectator( ).contains( user ) ) {
                event.setCancelled( true );
                return;

            }

            if ( GunGame.getInstance().getSpawn().containsLocation( player.getLocation() ) )
                event.setCancelled( true );

        }

    }

}
