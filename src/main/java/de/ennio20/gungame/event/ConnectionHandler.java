package de.ennio20.gungame.event;

import de.ennio20.gungame.GunGame;
import de.ennio20.gungame.util.User;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author Ennio20
 */
public class ConnectionHandler implements Listener {
    /**
     * Constructs the listener
     */
    public ConnectionHandler(  ) {
        GunGame.getInstance().getServer().getPluginManager().registerEvents( this, GunGame.getInstance() );

    }

    @EventHandler
    public void onJoin( PlayerJoinEvent event ) {
        final Player player = event.getPlayer();
        final Location location = new Location( Bukkit.getWorld( "world" ), 1033.5, 56, 191.5 );
        event.setJoinMessage( null );
        player.teleport( location );

        new User( player );

        player.setGameMode( GameMode.ADVENTURE );

        for ( User spec : GunGame.getInstance().getSpectator() )
            player.hidePlayer( spec.getSpigotPlayer() );

    }

    @EventHandler
    public void onQuit( PlayerQuitEvent event ) {
        final Player player = event.getPlayer();

        event.setQuitMessage( null );
        GunGame.getInstance().getUsers().removeIf( x -> x.getSpigotPlayer() == player );
        GunGame.getInstance().getSpectator().removeIf( x -> x.getSpigotPlayer() == player );

    }

}
