package de.ennio20.gungame.event;

import de.ennio20.gungame.GunGame;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;

/**
 * @author Ennio20
 */
public class MiscHandler implements Listener {
    /**
     * Constructs the listener
     */
    public MiscHandler( ) {
        GunGame.getInstance().getServer().getPluginManager().registerEvents( this, GunGame.getInstance() );

    }

    @EventHandler
    public void onClick( InventoryClickEvent event ) {
        final Player player = ( Player ) event.getWhoClicked();

        event.setCancelled( true );

        if ( event.getClickedInventory() == null )
            return;

        if ( event.getCurrentItem() == null )
            return;

        if ( event.getCurrentItem().getType().equals( Material.AIR ) )
            return;

        if ( event.getClickedInventory().getName().equals( "§7Tracker" ) ) {
            String name = event.getCurrentItem().getItemMeta().getDisplayName().replace( "§a", "" );
            final Player target = Bukkit.getPlayer( name );
            player.teleport( target );


        }

    }

    @EventHandler
    public void onDrop( PlayerDropItemEvent event ) {
        event.setCancelled( true );

    }

    @EventHandler
    public void onBlockBreak( BlockBreakEvent event ) {
        event.setCancelled( true );

    }

    @EventHandler
    public void onBlockPlace( BlockPlaceEvent event ) {
        event.setCancelled( true );

    }

    @EventHandler
    public void onPickup( PlayerPickupItemEvent event ) {
        event.setCancelled( true );

    }

    @EventHandler
    public void onItemSwap( PlayerSwapHandItemsEvent event ) {
        event.setCancelled( true );

    }

}
