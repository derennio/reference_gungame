package de.ennio20.gungame.event;

import de.ennio20.gungame.GunGame;
import de.ennio20.gungame.util.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * @author Ennio20
 */
public class ChatHandler implements Listener {
    /**
     * Constructs the listener
     */
    public ChatHandler(  ) {
        GunGame.getInstance().getServer().getPluginManager().registerEvents( this, GunGame.getInstance() );
    }

    @EventHandler
    public void on( AsyncPlayerChatEvent event ) {
        final Player player = event.getPlayer();
        final User user = GunGame.getInstance().getUser( player );

        event.setFormat( "§7(" + user.getLevel() + ") §a" + player.getName() + "§8:§f " + event.getMessage().replaceAll("%", "%%") );

    }

}
