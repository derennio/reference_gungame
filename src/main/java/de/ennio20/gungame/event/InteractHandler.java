package de.ennio20.gungame.event;

import de.ennio20.gungame.GunGame;
import de.ennio20.gungame.util.User;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * @author Ennio20
 */
public class InteractHandler implements Listener {
    /**
     * Constructs the listener
     */
    public InteractHandler( ) {
        GunGame.getInstance().getServer().getPluginManager().registerEvents( this, GunGame.getInstance() );

    }

    @EventHandler
    public void onInteract( PlayerInteractEvent event ) {
        final Player player = event.getPlayer();
        final User user = GunGame.getInstance().getUser( player );

        if ( player.getItemInHand() == null )
            return;

        if ( event.getAction().equals( Action.RIGHT_CLICK_AIR ) || event.getAction().equals( Action.RIGHT_CLICK_BLOCK ) ) {
            if ( player.getItemInHand().getType().equals( Material.COMPASS ) ) {
                if ( GunGame.getInstance().getSpectator().contains( user ) ) {
                    player.openInventory( GunGame.getInstance().getSpectatorinventory( player ) );

                }

            }

        }

    }

}
