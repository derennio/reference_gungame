package de.ennio20.gungame.event;

import de.ennio20.gungame.GunGame;
import de.ennio20.gungame.util.User;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * @author Ennio20
 */
public class MoveHandler implements Listener {
    /**
     * Constructs the listener
     */
    public MoveHandler( ) {
        GunGame.getInstance().getServer().getPluginManager().registerEvents( this, GunGame.getInstance() );

    }

    @EventHandler
    public void onMove( PlayerMoveEvent event ) {
        final Player player = event.getPlayer();

        if ( ( player.getLocation().getBlock().getType() == Material.STATIONARY_WATER ) ||
                        ( player.getLocation().getBlock().getType() == Material.STATIONARY_LAVA ) ||
                        ( player.getLocation().getBlock().getType() == Material.WATER ) ||
                        ( player.getLocation().getBlock().getType() == Material.LAVA ) ) {
            if ( !player.isDead() ) {
                User user = GunGame.getInstance().getUser( player );
                if ( GunGame.getInstance().getSpectator().contains( user ) )
                    return;

                player.setHealth( 0.0D );

            }
        }

    }

}
