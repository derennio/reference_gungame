package de.ennio20.gungame;

import com.google.gson.*;
import de.ennio20.gungame.command.SpecCMD;
import de.ennio20.gungame.event.*;
import de.ennio20.gungame.util.Region;
import de.ennio20.gungame.util.User;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ennio20
 */
public class GunGame extends JavaPlugin {
    private static GunGame instance;
    private List< User > users;
    private List< User > spectator;
    private JsonObject levelsFile;
    private File level;
    private Gson gson;
    private Region spawn;

    @Override
    public void onEnable ( ) {
        instance = this;

        this.users = new ArrayList<>(  );
        this.spectator = new ArrayList<>(  );

        this.gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
        this.level = new File( getDataFolder().getAbsolutePath() + "/levels.json" );

        if ( !this.level.exists() ) {
            try {
                this.createLevelfile();
            } catch ( IOException e ) {
                e.printStackTrace( );
            }
        }

        this.levelsFile = jsonObjectfromFile( this.level );

        new ConnectionHandler();
        new MoveHandler();
        new DamageHandler();
        new ChatHandler();
        new MiscHandler();
        new InteractHandler();

        new SpecCMD();

        final World world = Bukkit.getWorld( "world" );

        this.spawn = new Region( new Location( world, 1045, 75, 203 ), new Location( world, 1021, 50, 179 ) );

    }

    @Override
    public void onDisable ( ) {

    }

    /**
     * @return gungame instance
     */
    public static GunGame getInstance ( ) {
        return instance;
    }

    /**
     * @return ingame prefix for gungame
     */
    public String getPrefix ( ) {
        return "§7[§3GunGame§7] ";
    }

    /**
     * @return user list
     */
    public List< User > getUsers ( ) {
        return users;
    }

    /**
     * Method to get the User instance of a spigotplayer
     *
     * @param player to find user by spigotplayer
     * @return user match of spigotplayer
     */
    public User getUser( Player player ) {
        List< User > match = getUsers().stream().filter( x -> x.getSpigotPlayer() == player ).collect( Collectors.toList() );
        if ( match.size() == 1 )
            return match.get( 0 );

        return null;

    }

    /**
     * @return level file as json object
     */
    public JsonObject getLevelsFile ( ) {
        return levelsFile;
    }

    /**
     * @return the spawn area
     */
    public Region getSpawn ( ) {
        return spawn;
    }

    /**
     * @return every user in spectator mode
     */
    public List< User > getSpectator ( ) {
        return spectator;
    }

    /**
     * Method to create levels file
     * Properties specify the rewards for each level
     *
     * @throws IOException for file creation and for the filewriter
     */
    private void createLevelfile( ) throws IOException {
        if ( !this.level.createNewFile() )
            return;

        FileWriter fw = new FileWriter( this.level );

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty( "1", "WOOD_AXE" );
        jsonObject.addProperty( "2", "WOOD_SWORD" );
        jsonObject.addProperty( "3", "STONE_SWORD" );
        jsonObject.addProperty( "4", "IRON_SWORD" );
        jsonObject.addProperty( "5", "DIAMOND_SWORD" );

        fw.write( this.gson.toJson(jsonObject) );
        fw.close();

    }

    /**
     * Method to cast file to jsonobject
     *
     * @param file target file
     * @return jsonobject from file
     */
    private JsonObject jsonObjectfromFile( File file ){
        JsonObject jsonObject = new JsonObject();

        try {
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse( new FileReader( file ) );
            jsonObject = jsonElement.getAsJsonObject();

        } catch ( FileNotFoundException e ) {
            e.printStackTrace();
        }


        return jsonObject;
    }

    /**
     * @param player spectator player
     * @return inventory with the skull of every player
     */
    public Inventory getSpectatorinventory( Player player ) {
        Inventory inv = Bukkit.createInventory( null, 3 * 9, "§7Tracker" );

        for ( Player all : Bukkit.getOnlinePlayers() ) {
            if ( all != player && player.canSee( all ) ) {
                ItemStack itemStack = new ItemStack( Material.SKULL_ITEM, 1, ( short ) 3 );
                SkullMeta skullMeta = ( SkullMeta ) itemStack.getItemMeta();
                skullMeta.setDisplayName( "§a" + all.getName() );
                skullMeta.setOwner( all.getName() );
                itemStack.setItemMeta( skullMeta );
                inv.addItem( itemStack );

            }

        }

        return inv;

    }

}
